# Kubernetes tutorial

## Get a pool of virtual machines

Generate an SSH key that will be used by ansible to provision virtual machines.

```
make generate-ssh-key
```

This creates a folder secrets/ containing `id_rsa` and `id_rsa.pub`.

Virtual machines that will be created by vagrant are described in `./virtual-machines.yaml`. Fill free to adapt it to your needs. Then run

```
vagrant up
```

## Setup kubernetes cluster using ansible playbook

Ansible setup command is aliased in Makefile. You can use it like below

```
make setup-cluster
```

After cluster initialization, your kubernetes admin kubeconfig is stored in secrets/kubeconfig.
So you can [install kubectl](https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/), then 

```
export KUBECONFIG=secrets/kubeconfig
```

From now, you can check that nodes of your kubernetes cluster are up and have joined the cluster using

```
kubectl get nodes
```

You should see something like

```
NAME        STATUS   ROLES                  AGE   VERSION
manager-1   NotReady    control-plane,master   45m   v1.23.2
worker-1    NotReady    <none>                 44m   v1.23.2
worker-2    NotReady    <none>                 43m   v1.23.2
```

## Roadmap

Foreach /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

```
KUBELET_EXTRA_ARGS=--node-ip=192.168.19.X
```

Prevent kubelet to use generic address of the virtual machine (10.0.2.15).

## Install admin services

```
kubectl apply -f kubernetes/cluster/calico/
```

```
kubectl apply -f kubernetes/cluster/monitoring/
```

```
kubectl apply -f kubernetes/cluster/metallb/
```

```
kubectl apply -f kubernetes/cluster/traefik/
```

```
kubectl apply -f kubernetes/cluster/keycloak/
```

## Test that everything is working

To ensure everything is working, we are going to deploy 2 services
- whoami
- nginx

```
kubectl apply -f kubernetes/services/whoami.yaml
```

```
kubectl apply -f kubernetes/services/nginx.yaml
```
