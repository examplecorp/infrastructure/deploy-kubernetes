
generate-ssh-key:
	mkdir -p secrets/
	ssh-keygen -f secrets/id_rsa -q -N ""

setup-kubernetes:
	ANSIBLE_CONFIG=ansible/ansible.cfg ansible-playbook ansible/setup-kubernetes.yaml -i ansible/generate-inventory.py
