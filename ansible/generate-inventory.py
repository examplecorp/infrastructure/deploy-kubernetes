#!/usr/bin/python3

import json

with open('inventory.json') as config_file:
    configuration = json.load(config_file)

inventory = {"all": {"hosts": [], "children": ["managers", "workers"]}}
for group in ["managers", "workers"]:
    inventory[group] = {"hosts": [vm_config["ip"] for vm_config in configuration[group]], "children": [], "vars": {}}

print(json.dumps(inventory, indent=4))
